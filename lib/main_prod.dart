import 'package:flavors_codemagic/environment.dart';
import 'package:flavors_codemagic/main_common.dart';

Future<void> main() async {
  await mainCommon(Environment.prod);
}
